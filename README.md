# Tomcat Configuration

Public address is currently https://bpe.fairweben.de/camunda/app/welcome/default/ and https://bpe.fairweben.de/engine-rest

# Database Connection

In `/opt/tomcat/camunda-bpm-tomcat-7.12.0/conf/server.xml` delete H2 data source and replace it with this:
```xml
<Resource name="jdbc/ProcessEngine"
    auth="Container"
    type="javax.sql.DataSource" 
    factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
    uniqueResourceName="process-engine"
    driverClassName="org.mariadb.jdbc.Driver" 
    url="jdbc:mysql://localhost:3306/camunda?autoReconnect=true"
    defaultTransactionIsolation="READ_COMMITTED"
    username="camunda"  
    password="*****"
    maxActive="20"
    minIdle="10"
    maxIdle="20"
    maxAge="1800000" />
```

Provide the database driver `/opt/tomcat/camunda-bpm-tomcat-7.12.0/lib/mariadb-java-client-2.6.0.jar`

To allow additional characters as ID for User, Group and Tenant, edit the file `/opt/tomcat/camunda-bpm-tomcat-7.12.0/conf/bpm-platform.xml`  
and ad to this xPath `/bpm-platform/process-engine/properties` node the following:  
```xml
<property name="userResourceWhitelistPattern">[a-zA-Z0-9-_@\.]+</property>  
<property name="groupResourceWhitelistPattern">[a-zA-Z0-9-_@\.]+</property>  
<property name="tenantResourceWhitelistPattern">[a-zA-Z0-9-_@\.]+</property>  
```

# Enable security for REST service
Create a system user for camunda-bridge in camunda Admin GUI  
Add him to camunda-admin  group
Enable camunda-auth in `/opt/tomcat/camunda-bpm-tomcat-7.12.0/webapps/engine-rest/WEB-INF/web.xml` see (https://docs.camunda.org/manual/7.12/reference/rest/overview/authentication/)

# Provide custom service classes

Copy from this project (after building)  
    `target/dev/CustomServices/WEB-INF/classes`  
    `target/dev/CustomServices/WEB-INF/lib/*`  
to server directory  
    `/opt/tomcat/camunda-bpm-tomcat-7.12.0/lib/ext/`  

Change ownership of the `ext` dir  
> sudo chown -R tomcat:tomcat /opt/tomcat/camunda-bpm-tomcat-7.12.0/lib/ext

Add the following to the common.loader line in `/opt/tomcat/camunda-bpm-tomcat-7.12.0/conf/catalina.properties`  
> ,"${catalina.base}/lib/ext","${catalina.base}/lib/ext/*.jar"

Restart tomcat  
> systemctl restart tomcat
