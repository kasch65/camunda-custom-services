/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Stroke
{
  protected List<Sample> sample;

  protected AreaTypeEnum areaType;

  protected String areaName;

  protected Boolean dividesPage;

  protected Boolean bookPidget;

  public List<Sample> getSample()
  {
    return this.sample;
  }

  public void setSample(final List<Sample> sample)
  {
    this.sample = sample;
  }

  public AreaTypeEnum getAreaType()
  {
    return this.areaType;
  }

  public void setAreaType(final AreaTypeEnum areaType)
  {
    this.areaType = areaType;
  }

  public String getAreaName()
  {
    return this.areaName;
  }

  public void setAreaName(final String areaName)
  {
    this.areaName = areaName;
  }

  public Boolean getDividesPage()
  {
    return this.dividesPage;
  }

  public void setDividesPage(final Boolean dividesPage)
  {
    this.dividesPage = dividesPage;
  }

  public Boolean getBookPidget()
  {
    return this.bookPidget;
  }

  public void setBookPidget(final Boolean bookPidget)
  {
    this.bookPidget = bookPidget;
  }

}
