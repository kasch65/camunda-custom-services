/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public enum BarcodeTypeEnum {
  CODA_BAR,
  CODE_39,
  CODE_128,
  CODE_ITF,
  CODE_EAN_13,
  CODE_JAN_13,
  CODE_ISBN,
  CODE_OTHER;
}

