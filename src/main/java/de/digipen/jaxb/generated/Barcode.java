/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Barcode
{
  protected BarcodeTypeEnum type;

  protected String value;

  public BarcodeTypeEnum getType()
  {
    return this.type;
  }

  public void setType(final BarcodeTypeEnum type)
  {
    this.type = type;
  }

  public String getValue()
  {
    return this.value;
  }

  public void setValue(final String value)
  {
    this.value = value;
  }

}
