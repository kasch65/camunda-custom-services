/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Alt extends Entity
{
  protected String value;

  protected int score;

  public String getValue()
  {
    return this.value;
  }

  public void setValue(final String value)
  {
    this.value = value;
  }

  public int getScore()
  {
    return this.score;
  }

  public void setScore(final int score)
  {
    this.score = score;
  }

}
