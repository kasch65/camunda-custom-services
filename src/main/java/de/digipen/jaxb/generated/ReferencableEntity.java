//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2019.01.03 um 12:24:08 PM CET
//


package de.digipen.jaxb.generated;

public abstract class ReferencableEntity extends Entity
{
    protected String xId;

    public String getXId() {
        return this.xId;
    }

    public void setXId(final String value) {
        this.xId = value;
    }

}
