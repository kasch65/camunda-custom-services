/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Form
{
  protected List<Input> input;

  protected List<Control> control;

  protected Long formId;

  public List<Input> getInput()
  {
    return this.input;
  }

  public void setInput(final List<Input> input)
  {
    this.input = input;
  }

  public List<Control> getControl()
  {
    return this.control;
  }

  public void setControl(final List<Control> control)
  {
    this.control = control;
  }

  public Long getFormId()
  {
    return this.formId;
  }

  public void setFormId(final Long formId)
  {
    this.formId = formId;
  }

}
