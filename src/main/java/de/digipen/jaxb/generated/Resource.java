/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Resource extends Entity
{
  protected byte[] base64Content;

  protected ResourceMimetypeEnum type;

  protected Integer pdfPage;

  public byte[] getBase64Content()
  {
    return this.base64Content;
  }

  public void setBase64Content(final byte[] base64Content)
  {
    this.base64Content = base64Content;
  }

  public ResourceMimetypeEnum getType()
  {
    return this.type;
  }

  public void setType(final ResourceMimetypeEnum type)
  {
    this.type = type;
  }

  public Integer getPdfPage()
  {
    return this.pdfPage;
  }

  public void setPdfPage(final Integer pdfPage)
  {
    this.pdfPage = pdfPage;
  }

}
