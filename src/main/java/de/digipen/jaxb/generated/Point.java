/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Point extends Entity
{
  protected float x;
  protected float y;
  public float getX()
  {
    return this.x;
  }
  public void setX(final float x)
  {
    this.x = x;
  }
  public float getY()
  {
    return this.y;
  }
  public void setY(final float y)
  {
    this.y = y;
  }

}

