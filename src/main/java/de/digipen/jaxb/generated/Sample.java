/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Sample extends Point
{
  protected long t;

  protected byte f;

  public long getT()
  {
    return this.t;
  }

  public void setT(final long t)
  {
    this.t = t;
  }

  public byte getF()
  {
    return this.f;
  }

  public void setF(final byte f)
  {
    this.f = f;
  }

}
