/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Word extends Entity
{
  protected List<Alt> alt;

  public List<Alt> getAlt()
  {
    return this.alt;
  }

  public void setAlt(final List<Alt> alt)
  {
    this.alt = alt;
  }
}
