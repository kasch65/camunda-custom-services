/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public class Attribute extends Entity
{
  protected String value;

  protected Long id;

  protected String name;

  public String getValue()
  {
    return this.value;
  }

  public void setValue(final String value)
  {
    this.value = value;
  }

  @Override
  public Long getId()
  {
    return this.id;
  }

  @Override
  public void setId(final Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return this.name;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

}
