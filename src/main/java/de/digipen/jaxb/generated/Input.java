/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Input
{
  protected List<Stroke> stroke;
  protected List<Word> word;
  protected Barcode barcode;
  protected Resource resource;
  protected List<Attribute> attribute;
  protected Long permissionId;
  protected long startTime;
  protected long endTime;
  protected IcrEngineEnum icrEngine;
  protected Long deviceId;
  protected String pattern;
  protected Long formRegistrationId;
  protected String uservalue;
  protected Boolean boolValue;

  public List<Stroke> getStroke()
  {
    return this.stroke;
  }

  public void setStroke(final List<Stroke> stroke)
  {
    this.stroke = stroke;
  }

  public List<Word> getWord()
  {
    return this.word;
  }

  public void setWord(final List<Word> word)
  {
    this.word = word;
  }

  public Barcode getBarcode()
  {
    return this.barcode;
  }

  public void setBarcode(final Barcode barcode)
  {
    this.barcode = barcode;
  }

  public Resource getResource()
  {
    return this.resource;
  }

  public void setResource(final Resource resource)
  {
    this.resource = resource;
  }

  public List<Attribute> getAttribute()
  {
    return this.attribute;
  }

  public void setAttribute(final List<Attribute> attribute)
  {
    this.attribute = attribute;
  }

  public Long getPermissionId()
  {
    return this.permissionId;
  }

  public void setPermissionId(final Long permissionId)
  {
    this.permissionId = permissionId;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(final long startTime)
  {
    this.startTime = startTime;
  }

  public long getEndTime()
  {
    return this.endTime;
  }

  public void setEndTime(final long endTime)
  {
    this.endTime = endTime;
  }

  public IcrEngineEnum getIcrEngine()
  {
    return this.icrEngine;
  }

  public void setIcrEngine(final IcrEngineEnum icrEngine)
  {
    this.icrEngine = icrEngine;
  }

  public Long getDeviceId()
  {
    return this.deviceId;
  }

  public void setDeviceId(final Long deviceId)
  {
    this.deviceId = deviceId;
  }

  public String getPattern()
  {
    return this.pattern;
  }

  public void setPattern(final String pattern)
  {
    this.pattern = pattern;
  }

  public Long getFormRegistrationId()
  {
    return this.formRegistrationId;
  }

  public void setFormRegistrationId(final Long formRegistrationId)
  {
    this.formRegistrationId = formRegistrationId;
  }

  public String getUservalue()
  {
    return this.uservalue;
  }

  public void setUservalue(final String uservalue)
  {
    this.uservalue = uservalue;
  }

  public Boolean getBoolValue()
  {
    return this.boolValue;
  }

  public void setBoolValue(final Boolean boolValue)
  {
    this.boolValue = boolValue;
  }

}
