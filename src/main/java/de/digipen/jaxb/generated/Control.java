/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Control
{
  protected List<Input> input;

  protected Long formControlId;

  public List<Input> getInput()
  {
    return this.input;
  }

  public void setInput(final List<Input> input)
  {
    this.input = input;
  }

  public Long getFormControlId()
  {
    return this.formControlId;
  }

  public void setFormControlId(final Long formControlId)
  {
    this.formControlId = formControlId;
  }

}
