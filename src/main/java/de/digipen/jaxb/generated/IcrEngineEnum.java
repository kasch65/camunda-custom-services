/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

public enum IcrEngineEnum {
  MS_RECO, VO, DIVIDER, USER_INPUT, TESSERACT;
}
