/*
 * Copyright (c) 2018 EITCO GmbH
 * All rights reserved.
 *
 * Created on 22.02.2019
 *
 */
package de.digipen.jaxb.generated;

import java.util.List;

public class Formset extends RestrictedReferencableEntity
{
  protected List<Form> form;

  protected Long formsetId;

  public List<Form> getForm()
  {
    return this.form;
  }

  public void setForm(final List<Form> form)
  {
    this.form = form;
  }

  public Long getFormsetId()
  {
    return this.formsetId;
  }

  public void setFormsetId(final Long formsetId)
  {
    this.formsetId = formsetId;
  }

}
