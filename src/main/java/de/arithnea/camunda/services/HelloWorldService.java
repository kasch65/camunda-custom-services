package de.arithnea.camunda.services;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.camunda.spin.impl.json.jackson.JacksonJsonNode;

import de.digipen.jaxb.generated.Formset;

public class HelloWorldService implements JavaDelegate
 {

  //private static final Logger _LOGGER = Logger.getLogger(HelloWorldService.class);
  private static final Log LOGGER = LogFactory.getLog(HelloWorldService.class);


  public HelloWorldService()
   {
    super();
    LOGGER.info("Service created.");
   }


  @Override
  public void execute(final DelegateExecution execution) throws Exception
   {
    LOGGER.info(String.format("Service called."));
    final ExecutionEntity processInstance = (ExecutionEntity)execution.getProcessInstance();
    final ProcessDefinitionEntity processDefinition = processInstance.getProcessDefinition();
    LOGGER.info(String.format("Activity: %s: %s", processDefinition.getName(), execution.getCurrentActivityName()));
    boolean throwException = false;
    final Map<String, Object> vars = execution.getVariables();
    for (final Entry<String, Object> var : vars.entrySet())
     {
      try
       {
        if (var.getValue() != null)
         {
          LOGGER.info(String.format("Instance variable: %s = %s(%s)", var.getKey(), var.getValue().getClass().getName(), var.getValue()));
          // Normalize org.camunda.spin.impl.json.jackson.JacksonJsonNode
          if (var.getValue() instanceof JacksonJsonNode)
           {
            final JacksonJsonNode jsonNode = (JacksonJsonNode)var.getValue();
            try
             {
              final Formset formset = jsonNode.mapTo(Formset.class);
              LOGGER.info(String.format("JSon Book representation: %s = %s(%s)", var.getKey(), formset.getClass().getName(), formset));
             }
            catch (final Exception e)
             {
              LOGGER.info(String.format("Can't convert JSon representation to Book!"), e);
             }
           }
         }
        else
         {
          LOGGER.info(String.format("Instance variable: %s = null", var.getKey()));
         }
        if ("next".equals(var.getKey()) && "exportError".equals(var.getValue()))
         {
          throwException = true;
         }
       }
      catch (final NullPointerException e)
       {
        LOGGER.info(String.format("Failed to print variable %s ?(%s)!", var.getKey(), var.getValue()), e);
       }
     }
    if (throwException)
     {
      LOGGER.info(String.format("Throwing Exception \"impossible\"..."));
      execution.setVariable("exception", String.format("Activity: %s: %s ist unmöglich", processDefinition.getName(), execution.getCurrentActivityName()));
      throw new BpmnError("impossible");
     }
    execution.removeVariable("exception");
    setNote(execution, String.format("%s: Daten exportiert.", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
   }


  public void addNote(final DelegateExecution execution, final String noteText)
   {
    final Object notesObj = execution.getVariable("notes");
    String notes = null;
    if (notesObj != null && notesObj instanceof String && !((String)notesObj).isEmpty())
     {
      notes = (String)notesObj + '\n' + noteText;
     }
    else
     {
      notes = noteText;
     }
    execution.setVariable("notes", notes);
   }


  public void setNote(final DelegateExecution execution, final String noteText)
   {
    execution.setVariable("notes", noteText);
   }

 }
