package de.arithnea.camunda.services;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class GenericSpringService implements JavaDelegate
 {

  private static final Log LOGGER = LogFactory.getLog(GenericSpringService.class);
  private static BeanFactory appContext;


  public GenericSpringService()
   {
    super();
    if (appContext == null)
     {
      appContext = new ClassPathXmlApplicationContext("/spring/spring.xml");
      LOGGER.info("Spring service context created.");
     }
    LOGGER.info("Service created.");
   }


  @Override
  public void execute(final DelegateExecution execution) throws Exception
   {
    LOGGER.info(String.format("Service called."));
    final Object serviceBeanObject = appContext.getBean(execution.getCurrentActivityName());
    if (!(serviceBeanObject instanceof JavaDelegate))
     {
      throw new Exception(String.format("There is no bean with id=%s implementing JavaDelegate!", execution.getCurrentActivityName()));
     }
    final JavaDelegate serviceBean = (JavaDelegate)serviceBeanObject;
    serviceBean.execute(execution);
   }

 }
