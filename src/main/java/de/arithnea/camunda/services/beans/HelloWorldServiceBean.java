package de.arithnea.camunda.services.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;

public class HelloWorldServiceBean implements JavaDelegate
 {

  private static final Log LOGGER = LogFactory.getLog(HelloWorldServiceBean.class);


  public HelloWorldServiceBean()
   {
    super();
    LOGGER.info("Bean instance created.");
   }


  @Override
  public void execute(final DelegateExecution execution) throws Exception
   {
    LOGGER.info(String.format("Service called."));
    final ExecutionEntity processInstance = (ExecutionEntity)execution.getProcessInstance();
    final ProcessDefinitionEntity processDefinition = processInstance.getProcessDefinition();
    LOGGER.info(String.format("Activity: %s: %s", processDefinition.getName(), execution.getCurrentActivityName()));
    boolean throwException = false;
    final Map<String, Object> vars = execution.getVariables();
    for (final Entry<String, Object> var : vars.entrySet())
     {
      if (var.getValue() != null)
       {
        LOGGER.info(String.format("Instance variable: %s = %s(%s)", var.getKey(), var.getValue().getClass().getName(), var.getValue()));
       }
      else
       {
        LOGGER.info(String.format("Instance variable: %s = null", var.getKey()));
       }
      if ("next".equals(var.getKey()) && "exportError".equals(var.getValue()))
       {
        throwException = true;
       }
     }
    if (throwException)
     {
      LOGGER.info(String.format("Throwing Exception..."));
      throw new Exception("Expected Exception thrown.");
     }
    setNote(execution, String.format("%s: Daten exportiert.", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
   }


  public void addNote(final DelegateExecution execution, final String noteText)
   {
    final Object notesObj = execution.getVariable("notes");
    String notes = null;
    if (notesObj != null && notesObj instanceof String && !((String)notesObj).isEmpty())
     {
      notes = (String)notesObj + '\n' + noteText;
     }
    else
     {
      notes = noteText;
     }
    execution.setVariable("notes", notes);
   }


  public void setNote(final DelegateExecution execution, final String noteText)
   {
    execution.setVariable("notes", noteText);
   }

 }
